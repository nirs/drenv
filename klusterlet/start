#!/usr/bin/env -S python3 -u

# SPDX-FileCopyrightText: The RamenDR authors
#
# SPDX-License-Identifier: Apache-2.0

import os
import sys
from string import Template

import drenv

NAMESPACE = "open-cluster-management-agent"


def deploy_klusterlet(cluster):
    drenv.log_progress(f"Deploying klusterlet subscription to cluster {cluster}")
    drenv.kubectl(
        "apply",
        "--filename", "https://operatorhub.io/install/klusterlet.yaml",
        profile=cluster,
    )


def wait_until_klusterlet_is_rolled_out(cluster):
    drenv.wait_for(
        "csv/klusterlet.v0.8.0",
        output="jsonpath={.status.phase}",
        namespace="operators",
        profile=cluster,
    )

    drenv.log_progress(f"Waiting until klusterlet succeeds in cluster {cluster}")
    drenv.kubectl(
        "wait", "csv/klusterlet.v0.8.0",
        "--for", "jsonpath={.status.phase}=Succeeded",
        "--namespace", "operators",
        "--timeout", "300s",
        profile=cluster,
    )

    drenv.log_progress(f"Waiting for klusterlet rollout in cluster {cluster}")
    drenv.kubectl(
        "rollout", "status", "deployment/klusterlet",
        "--namespace", "operators",
        "--timeout", "300s",
        profile=cluster,
    )


def create_klusterlet_instance(cluster, hub):
    drenv.log_progress(f"Creating namespace {NAMESPACE} in cluster {cluster}")
    namespace_yaml = drenv.kubectl(
        "create", "namespace", NAMESPACE,
        "--dry-run=client",
        "--output=yaml",
        verbose=False,
        profile=cluster,
    )
    drenv.kubectl(
        "apply",
        "--filename", "-",
        input=namespace_yaml,
        profile=cluster,
    )

    drenv.log_progress(
        f"Create secret boostrap-hub-kubeconfig in cluster {cluster}"
    )
    hub_kubeconfig = os.path.join(drenv.config_dir(hub), "kubeconfig")
    secret_yaml = drenv.kubectl(
        "create", "secret", "generic", "bootstrap-hub-kubeconfig",
        f"--from-file=kubeconfig={hub_kubeconfig}",
        f"--namespace={NAMESPACE}",
        "--dry-run=client",
        "--output=yaml",
        verbose=False,
        profile=cluster,
    )
    drenv.kubectl(
        "apply",
        "--filename", "-",
        input=secret_yaml,
        profile=cluster,
    )

    with open("klusterlet/klusterlet.yaml") as f:
        klusterlet_template = Template(f.read())

    klusterlet_yaml = klusterlet_template.substitute(
        clusterName=cluster,
        namespace=NAMESPACE,
    )

    drenv.log_progress(f"Creating klusterlet instance in cluster {cluster}")
    drenv.kubectl(
        "apply",
        "--filename", "-",
        input=klusterlet_yaml,
        profile=cluster,
    )


def wait_until_instance_is_registered(cluster, hub):
    drenv.wait_for(
        "deployment/klusterlet-registration-agent",
        namespace="open-cluster-management-agent",
        profile=cluster,
    )

    drenv.log_progress(
        "Waiting until klusterlet registration agent deployment "
        f"is avaialble in cluster {cluster}"
    )
    drenv.kubectl(
        "wait", "deployment/klusterlet-registration-agent",
        "--for", "condition=available",
        "--namespace", "open-cluster-management-agent",
        "--timeout", "300s",
        profile=cluster,
    )

    drenv.wait_for(
        f"managedclusters/{cluster}",
        timeout=30,
        profile=hub,
    )


def approve_registration(cluster, hub):
    drenv.log_progress(f"Fetch certificate signning request for {cluster}")
    csr = drenv.kubectl(
        "get", "csr",
        "--selector",  f"open-cluster-management.io/cluster-name={cluster}",
        "--field-selector", "spec.signerName=kubernetes.io/kube-apiserver-client",
        "--output", "name",
        profile=hub,
    )

    drenv.log_progress(f"Approve certificate signning request for {cluster}")
    drenv.kubectl("certificate", "approve", csr, profile=hub)

    drenv.log_progress(f"Acccepting managed cluster {cluster}")
    drenv.kubectl(
        "patch", f"managedclusters/{cluster}",
        "--patch", '{"spec":{"hubAcceptsClient":true}}',
        "--type", "merge",
        profile=hub,
    )


def wait_until_work_agent_available(cluster, hub):
    drenv.log_progress(
        f"Waiting until managed cluster {cluster} is available"
    )
    drenv.kubectl(
        "wait", f"managedclusters/{cluster}",
        "--for", "condition=ManagedClusterConditionAvailable",
        "--timeout", "60s",
        profile=hub,
    )

    drenv.log_progress(
        f"Waiting until klusterlet work agent for {cluster} is available"
    )
    drenv.kubectl(
        "wait", "deployment/klusterlet-work-agent",
        "--for", "condition=available",
        "--namespace", NAMESPACE,
        "--timeout", "90s",
        profile=cluster,
    )


if len(sys.argv) != 4:
    print(f"Usage: {sys.argv[0]} cluster1 cluster2 hub")
    sys.exit(1)

cluster1 = sys.argv[1]
cluster2 = sys.argv[2]
hub = sys.argv[3]

deploy_klusterlet(cluster1)
deploy_klusterlet(cluster2)

wait_until_klusterlet_is_rolled_out(cluster1)
wait_until_klusterlet_is_rolled_out(cluster2)

create_klusterlet_instance(cluster1, hub)
create_klusterlet_instance(cluster2, hub)

wait_until_instance_is_registered(cluster1, hub)
wait_until_instance_is_registered(cluster2, hub)

approve_registration(cluster1, hub)
approve_registration(cluster2, hub)

wait_until_work_agent_available(cluster1, hub)
wait_until_work_agent_available(cluster2, hub)
